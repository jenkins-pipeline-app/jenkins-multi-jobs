#FROM openshift/nginx:1.12
FROM registry.access.redhat.com/rhscl/nginx-112-rhel7
COPY build /opt/app-root/src
WORKDIR /usr/share/container-scripts/nginx
#EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
